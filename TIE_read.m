clear
clc
close all

%% 

% maindir = '/home/s3551189/Workspace/Topside/ISR/TIEGCM/TIEGCM_results';
maindir = 'Data/TIEGCM'; % where you put your TIEGCM file
subdir  = dir( maindir );

mon = 'jun';

TE_clu = [];
num_clu = [];

% for i = 1 : length( subdir )
%     if( isequal( subdir( i ).name, '.' )||...
%         isequal( subdir( i ).name, '..')||...
%         ~subdir( i ).isdir)               % 如果不是目录则跳过
%         continue;
%     end
%     subdirpath = fullfile(subdir.name, '*.nc' );
%     dat = dir( subdir )               % 子文件夹下找后缀为dat的文件

for j = 1 : length( subdir )
        subdir( j ).name
        if( isequal( subdir( j ).name, '.' )||...
            isequal( subdir( j ).name, '..'))    % 如果不是目录则跳过
            continue;
        end
        path = fullfile( maindir, subdir( j ).name);
        
        if(~strcmp(path(end-15:end-12), 'sech') | ~strcmp(path(end-27:end-25), mon))
            continue;
        end
%         fid = fopen( datpath );
        nc_info = ncinfo(path);
        % 此处添加你的对文件读写操作 %
%         time = ncread(path, 'time');
        ut = ncread(path, 'ut');
        lon = ncread(path, 'lon');
        lat = ncread(path, 'lat');
        lev = ncread(path, 'lev');
%         p0 = ncread(path, 'p0');
%         p0_m = ncread(path, 'p0_model');
        Ti = ncread(path, 'TI');
        Te = ncread(path, 'TE');
        Ne = ncread(path, 'NE');
        Z = ncread(path, 'ZGMID')/1e5;
        
        f107d = ncread(path, 'f107d');
        f107a = ncread(path, 'f107a');
        P = (f107a + f107d)/2;
        Kp = ncread(path, 'Kp');
        
         ind = find(f107d <120);
         
         MI = zeros(size(Ne));
         
         for a = 1:size(Ne,1)
             for b = 1:size(Ne,2)
                 for d = 1:size(Ne,4)
                     [M,I] = max(Ne(a,b,:,d));
%                      if(length(I)>1)
%                          I = I(1);
%                      end
                 for c = I:size(Ne,3)
                     MI(a,b,c,d) = 1;
                 end
                 
                 end
             end
         end
                     
        
%          ut = ut(find(Kp<10 & Kp>2));
        
%         ut = time/60;
        [Lon, Lat, Lev, UT] = ndgrid(lon, lat, lev, ut(ind));
        
        Lon = reshape(Lon, [], 1);
        Lat = reshape(Lat, [], 1);
%         Lev = reshape(Lev, [], 1);
        UT = reshape(UT, [], 1);
        Te = reshape(Te(:,:,:,ind), [], 1);
        Ti = reshape(Ti(:,:,:,ind), [], 1);
        Z = reshape(Z(:,:,:,ind), [], 1);
        MI = reshape(MI(:,:,:,ind), [], 1);  
        
        tic
         %t_zone = timezone(Lon,'degrees');
        toc
        
         LT = UT + Lon/15;
%         LT  = UT;
        LT(find(LT>=24)) = rem(LT(find(LT>=24)), 24);
        LT(find(LT<0)) = LT(find(LT<0))+24;     
        
        
        
%         tic
%         
%         Z(find(Z>1000)) = 0;
%         for num = 1:length(Z)
%             [B,H,D,I,F] = igrfmagm(Z(num),Lat(num),Lon(num),decyear(2009,12,22), 12);
%             geom_lat(num) = atan(tan(I*pi/180)/2)*180/pi;
%         end
%         toc
%       toc
%                 % low latitude (narrow down to around Arecibo)
%         if (median(geom_lat{i})>30 | median(geom_lat{i})<10 ...
%             | median(data_lon{i})>180 | median(data_lon{i})<-180)
%             continue;
%         end
        
        time_res = 1.5;
        h_res = 20;
        TE = zeros(400/h_res+1, 24/time_res+1);
        num = zeros(400/h_res+1, 24/time_res+1);
        
        
        for m = 0:time_res:24                                                
            for n = 200:h_res:600
                index = find(LT >= m & LT < m+time_res & Z >= n & Z < n+h_res...
                    & Lat<=60 & Lat>=30 & Lon<=180 & Lon>=-180 & Te<1e5...
                    & MI==1);
                if isempty(index)
                    continue;
                end
                TE((n-200)/h_res+1, m/time_res+1) = mean(Te(index));
                num((n-200)/h_res+1, m/time_res+1) = length(index);
                
            end
        end  
        
            
        x = 0:time_res:24;
        y = 200:h_res:600;
%         y = fliplr(y);
       
        [X,Y] = meshgrid(x,y');
        
        % save TE    
         save datum/mid_sum.mat TE
      
        TE_clu = [TE_clu reshape(TE, [], 1)];
        num_clu = [num_clu reshape(num, [], 1)];
               
        TE(find(TE == 0)) = nan;
        num_clu(find(num_clu == 0)) = nan;
        

        
        close all
        figure; contourf(X, Y, TE);
        caxis([500,3000])
        colormap(jet)
        colorbar('Ticks',[500,1000,1500,2000,2500,3000]);
        % shading flat
        xlabel('Local Time (hours)')
        ylabel('Height (km)')
        
        figure; contourf(X, Y, num);
%         colorbar('Ticks',[500,1000,1500,2000,2500]);
        % shading flat
        colorbar
        xlabel('Local Time (hours)')
        ylabel('Height (km)')
%         savefig(['Fig/', num2str(j), '.fig']);

%         p = p0*exp(-lev)*100;
% %         h = (1-(p/1013.25)*exp(0.190284))*145366.45;
%         h = atmospalt(p);
end
   