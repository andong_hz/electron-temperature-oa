########################## import modules ###############################

import json
import shutil
from shutil import copyfile
import os
import sys
import ipdb
import numpy as np
import scipy.io as sio
import time
import torch

from datetime import datetime
import iri2016 as iri
from argparse import ArgumentParser
from matplotlib.pyplot import show
import iri2016.plots as piri
from multiprocessing import cpu_count, Pool
import progressbar
import tqdm

# import Src
sys.path.append("Src")
from Py_Fun import Project, Print_test, Te_IRI_obj, network_develop

if not os.path.isdir('Projects/'):
    os.mkdir('Projects')

########################## Read Configurations ###############################
#%matplotlib notebook

# Read Config file
with open('Config.json') as json_data_file:
    Config = json.load(json_data_file)

X_select_keys = [key for key in Config['X_select'].keys()]
X_select_values = [value for value in Config['X_select'].values()]

Y_select_keys = [key for key in Config['Y_select'].keys()]
Y_select_values = [value for value in Config['Y_select'].values()]

Stations = [key for key in Config['Test_num'].keys()]
Test_nums = [value for value in Config['Test_num'].values()]

Format_keys = [key for key in Config['Figure_format'].keys()]
Format_values = [value for value in Config['Figure_format'].values()]

# Read Global Coefficients
#with open('Global.json') as Global_coef:
#    Global = json.load(Global_coef)

# Read Project name, ISR datasets and thresholds for preprocessing
Project_name = Config['Names']['Project']
ISR_name = Config['Names']['ISR']
ISR_range = Config['Para_range']

# Read which variable has been picked
index_X = []
index_Y = []


#ipdb.set_trace()

for i in range(len(X_select_keys)):
    if X_select_values[i]:
        index_X.append(i)
for i in range(len(Y_select_keys)):
    if Y_select_values[i]:
        index_Y.append(i)

########################## Project ###############################

project = Project(Project_name)
project.create()

# remove the project
#project.remove()

Print_test(ISR_range)
time.sleep(1)
########################## ISR data precprocess ###############################


if Config['Flags']['ISR_preprocess']:

    # Read and precprocess ISR data
    Out = project.ISR_Read()
    X = Out[0]
    Y = Out[1]
    Ref = Out[2]

    index_X = np.asarray(index_X).squeeze()
    index_Y = np.asarray(index_Y).squeeze()

    Print_test(index_Y)
    Print_test(Y.shape)

    # Variable Selection
    X_t = X[index_X,:]
    Y_t = Y[index_Y,:]

    # Normlisation
    X,Y = project.Normlise(X_t, Y_t)
    Y = Y.unsqueeze(0)
    #Print_test(X.shape)
    #Print_test(Y.shape)

    ########################## Tensor to Dataset ###############################
    #print(X.shape)
    #print(Y.shape)
    #print(Ref.shape)

    _ , _ = project.Dataset_create(X, Y, torch.from_numpy(Ref))
    #Print_test(Out_train)
    #Print_test(Out_test)
    time.sleep(.5)

########################## Modelling ##################################
# to make it simple, change index_Y.shape to 1
if Config['Flags']['Modelling']:
    #Print_test(len(index_Y))
    net = project.Modelling(len(index_X), 1)
    Print_test(net)
    time.sleep(.5)

########################## Statistical Accuracy ##################################
if Config['Flags']['Accuracy']:

    Acc = np.zeros(len(Stations))
    for i in range(len(Stations)):
        Acc[i] = project.Accuracy(i)
    Print_test(Acc)
    #sio.savemat()
    time.sleep(.5)

########################## Modelling Comparison ##################################
"""
Te Model results comparison

TBT-2012:
Brace-78:
DNN model:
ISR observation:

"""
if Config['Flags']['Comparison']:

    X_t, Y_t, Ref, num_clu = project.Comparison()
    Te_DNN = project.Predict(X_t, index_X).squeeze()

    Print_test(X_t.shape)
    #del X_t
    #del Y_t

    Print_test(['Te_DNN.shape=', Te_DNN.shape])
    Print_test(['Ref.shape=', Ref.shape])

    #ipdb.set_trace()

    cores = cpu_count()
    p = Pool(cores)
    Print_test(cores)


     #no CPU calculation

    progress = progressbar.ProgressBar()
    """

    Te_noCPU = np.zeros([2,X_t.shape[1]])
    for i in progress(range(X_t.shape[1])):
        Te_noCPU[:,i] = a.Te_IRI_Read(i)
        print(a.Te_IRI_Read(i))

    """
    Te_CPU = []
    a = Te_IRI_obj(Ref)
    print('Start to generate Te datasets from IRI-2016...')
    time.sleep(.5)
    for t in tqdm.tqdm(p.imap_unordered(a.Te_IRI_Read, range(Ref.shape[1])) \
                       , total=Ref.shape[1]):
        Te_CPU.append(t)

    #Te = p.map(a.Te_IRI_Read, progress(range(X_t.shape[1]))) #X_t.shape[1]-130000)
    Print_test(np.asarray(Te_CPU))
    Te = np.asarray(Te_CPU).squeeze()
    Te_TBT = Te[:,0]
    Te_Brace = Te[:,1]

    #ipdb.set_trace()
    # save Te results for all
    for i in range(len(Stations)):

        Te_TBT_t = Te_TBT[num_clu[i]:num_clu[i+1]]
        Te_ISR_t = Y_t[num_clu[i]:num_clu[i+1]]
        Te_Brace_t = Te_Brace[num_clu[i]:num_clu[i+1]]
        Te_ref = Ref[:,num_clu[i]:num_clu[i+1]]
        Te_DNN_t = Te_DNN[num_clu[i]:num_clu[i+1]]

        num_t = num_clu[i+1]-num_clu[i]

        sio.savemat('Projects/'+Project_name+'/Data/Output/'\
                    + Stations[i]+'_Te_outputs.mat', \
                    {'Te_TBT':Te_TBT_t, \
                     'Te_ISR':Te_ISR_t, \
                     'Te_DNN':Te_DNN_t, \
                     'Te_Brace':Te_Brace_t, \
                     'Te_ref':Te_ref, \
                     'Te_num':num_t})
        #ipdb.set_trace()
        #project.move(Stations[i]+'_Te_outputs.mat',\
        #             'Data/', 'Data/Output/')

    time.sleep(.5)

########################## Figures Generation ##################################

if Config['Flags']['Generation']:

    project.Show_case()


########################## RO prediction ##################################

if Config['Flags']['RO_predict']:

    project.RO_predict(index_X)
